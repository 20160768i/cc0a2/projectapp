package pe.edu.uni.sperezr.postitapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class ViewNoteActivity extends AppCompatActivity {

    TextView textViewNoteDate, textViewNoteTitle, textViewContent;
    int number, position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_note);

        textViewNoteDate = findViewById(R.id.text_view_note_date);
        textViewNoteTitle = findViewById(R.id.text_view_note_title);
        textViewContent = findViewById(R.id.text_view_note_content);

        number = getIntent().getIntExtra("number_to_note",0);
        position = getIntent().getIntExtra("position_to_note", 0);
        textViewNoteTitle.setText(MainActivity.categories.get(number).getNotes().get(position).getTitle());
        textViewContent.setText(MainActivity.categories.get(number).getNotes().get(position).getContent());
    }
}