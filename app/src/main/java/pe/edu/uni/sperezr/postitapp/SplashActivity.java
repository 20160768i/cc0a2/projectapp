package pe.edu.uni.sperezr.postitapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends Activity {
    ImageView imageViewBlackLine, imageViewRedLine, imageViewYellowLine,
            imageViewGreenLine, imageViewBlueLine;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageViewBlackLine = findViewById(R.id.image_view_splash_black_line);
        imageViewBlackLine.setAnimation(AnimationUtils.loadAnimation(this, R.anim.black_line_animation));
        imageViewRedLine = findViewById(R.id.image_view_splash_red_line);
        imageViewRedLine.setAnimation(AnimationUtils.loadAnimation(this, R.anim.red_line_animation));
        imageViewYellowLine = findViewById(R.id.image_view_splash_yellow_line);
        imageViewYellowLine.setAnimation(AnimationUtils.loadAnimation(this, R.anim.yellow_line_animation));
        imageViewGreenLine = findViewById(R.id.image_view_splash_green_line);
        imageViewGreenLine.setAnimation(AnimationUtils.loadAnimation(this, R.anim.green_line_animation));
        imageViewBlueLine = findViewById(R.id.image_view_splash_blue_line);
        imageViewBlueLine.setAnimation(AnimationUtils.loadAnimation(this, R.anim.blue_line_animation));
        textView = findViewById(R.id.text_view_splash);
        textView.setAnimation(AnimationUtils.loadAnimation(this, R.anim.text_animation));

        new CountDownTimer(5000, 1000){

            @Override
            public void onTick(long millisUntilFinished) { }

            @Override
            public void onFinish() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();
    }
}