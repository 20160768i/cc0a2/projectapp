package pe.edu.uni.sperezr.postitapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class CreateNoteActivity extends AppCompatActivity {
    EditText editTextNoteTitle, editTextNoteContent;
    Spinner spinnerCategories;
    Button buttonCreateNote;
    ImageView imageViewCategoryColor;

    ArrayAdapter<String> adapter;
    static ArrayList<String> categoryNotesNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);

        editTextNoteTitle = findViewById(R.id.edit_text_note_title);
        editTextNoteContent = findViewById(R.id.edit_text_note_content);
        spinnerCategories = findViewById(R.id.spinner_categories);
        buttonCreateNote = findViewById(R.id.button_create_note);
        imageViewCategoryColor = findViewById(R.id.image_view_category_color);

        updateAdapter();
        adapter = new ArrayAdapter<>(CreateNoteActivity.this, android.R.layout.simple_spinner_item, categoryNotesNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategories.setAdapter(adapter);
        spinnerCategories.setSelection(0);
        spinnerCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.notifyDataSetChanged();
                if(position == categoryNotesNames.size() - 1){
                    Intent intent = new Intent(CreateNoteActivity.this, CreateCategoryActivity.class);
                    startActivity(intent);
                    spinnerCategories.setSelection(0);

                }else{
                    int color = MainActivity.categories.get(position).getColor();
                    if(color < 0) //Color as aRBG
                        imageViewCategoryColor.setImageTintList(ColorStateList.valueOf(color));
                    else //Color as Resource
                        imageViewCategoryColor.setImageTintList(ColorStateList.valueOf(
                            ContextCompat.getColor(CreateNoteActivity.this, color)));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonCreateNote.setOnClickListener(v -> {
            String sTitle, sContent;
            sTitle = editTextNoteTitle.getText().toString();
            sContent = editTextNoteContent.getText().toString();
            if(sTitle.isEmpty()){
                Toast.makeText(getApplicationContext(), R.string.toast_msg_note_title_is_empty,
                        Toast.LENGTH_LONG).show();
                return;
            }

            if(sContent.isEmpty()){
                Toast.makeText(getApplicationContext(), R.string.toast_msg_note_content_is_empty,
                        Toast.LENGTH_LONG).show();
                return;
            }

            Note note = new Note(sTitle, sContent, MainActivity.categories.get(spinnerCategories.getSelectedItemPosition()).getColor());
            if(MainActivity.categories.get(spinnerCategories.getSelectedItemPosition()).addNote(note)){
                Toast.makeText(getApplicationContext(), "Se añadio la nota a la categoria", Toast.LENGTH_LONG).show();
                finish();
            }else{
                Toast.makeText(getApplicationContext(), "no se pudo añadir la nota a la categoria", Toast.LENGTH_LONG).show();
            }

        });
    }

    public static void updateAdapter(){
        if(categoryNotesNames == null)
            categoryNotesNames = new ArrayList<>();
        else
            categoryNotesNames.clear();
        for(CategoryNote category : MainActivity.categories){
            categoryNotesNames.add(category.getTitle());
        }
        categoryNotesNames.add("Create a new category");
    }
}
