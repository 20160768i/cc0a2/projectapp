package pe.edu.uni.sperezr.postitapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Serializable {
    FloatingActionButton fabMenu, fabAddNote, fabCreateCategory, fabRefreshView, fabSaveData;
    TextView textViewAddNote, textViewCreateCategory, textViewRefreshView, textViewSaveData;
    TextView textViewCount;
    GridView gridView;

    boolean isMenuOpen = false;
    public static ArrayList<CategoryNote> categories = new ArrayList<>();
    GridAdapter gridAdapter;

    SharedPreferences sharedPreferences;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewCount = findViewById(R.id.text_view_count);

        fabMenu = findViewById(R.id.fab_menu);
        fabAddNote = findViewById(R.id.fab_add_note);
        fabCreateCategory = findViewById(R.id.fab_create_category);
        fabRefreshView = findViewById(R.id.fab_refresh_view);
        fabSaveData = findViewById(R.id.fab_save_data);

        textViewAddNote = findViewById(R.id.text_view_add_note);
        textViewCreateCategory = findViewById(R.id.text_view_create_category);
        textViewRefreshView = findViewById(R.id.text_view_refresh_view);
        textViewSaveData = findViewById(R.id.text_view_save_data);

        retrieveData();

        gridView = findViewById(R.id.grid_view);
        gridAdapter = new GridAdapter(this);
        gridView.setAdapter(gridAdapter);
        gridView.setOnItemClickListener((parent, view, position, id) -> {
            ArrayList<String> notes = categories.get(position).arrayNotesTitles();
            if(notes == null)
                Toast.makeText(getApplicationContext(), R.string.toast_msg_empty_category, Toast.LENGTH_SHORT).show();
            else {
                Intent intent = new Intent(MainActivity.this, ViewCategoryActivity.class);
                intent.putExtra("position_number", position);
                startActivity(intent);
            }
        });
        gridView.setOnItemLongClickListener((parent, view, position, id) -> {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle(R.string.dialog_title)
                    .setMessage(R.string.dialog_text)
                    .setNegativeButton(R.string.no, (dialog, which) -> dialog.cancel())
                    .setPositiveButton(R.string.yes, (dialog, which) -> {
                        deleteCategory(position);
                        gridAdapter.notifyDataSetChanged();
                    })
                    .show();
            alertDialog.create();
            return true;
        });

        fabMenu.setOnClickListener(v -> {
            if(count++ >= 17){
                sharedPreferences.edit().clear().apply();
                count = 0;
                Toast.makeText(getApplicationContext(), "DATA DELETED", Toast.LENGTH_SHORT).show();
            }

            if(!isMenuOpen) showMenu();
            else closeMenu();
        });

        fabAddNote.setOnClickListener(v -> {
            closeMenu();
            count = 0;
            Intent intent = new Intent(this, CreateNoteActivity.class);
            startActivity(intent);
        });

        fabCreateCategory.setOnClickListener(v -> {
            closeMenu();
            count = 0;
            Intent intent = new Intent(this, CreateCategoryActivity.class);
            startActivity(intent);

        });

        fabRefreshView.setOnClickListener(v -> {
            count = 0;
            Toast.makeText(getApplicationContext(), R.string.toast_msg_view_updated, Toast.LENGTH_SHORT).show();
            gridAdapter.notifyDataSetChanged();
        });

        fabSaveData.setOnClickListener(v -> {
            count = 0;
            saveData();
        });

    }

    private void saveData(){
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(categories);
        editor.putString("key_categories", json);
        editor.apply();

        Toast.makeText(getApplicationContext(), R.string.toast_msg_save_data, Toast.LENGTH_LONG).show();
    }

    private void retrieveData(){
        Gson gson = new Gson();
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        String json = sharedPreferences.getString("key_categories", "");
        Type type = new TypeToken<ArrayList<CategoryNote>>(){}.getType();
        categories = gson.fromJson(json, type);
        if(categories == null)
            fillCategories();
        else {
            Toast.makeText(getApplicationContext(), R.string.toast_msg_load_data, Toast.LENGTH_SHORT).show();
            textViewCount.setVisibility(View.INVISIBLE);
        }
    }

    private void deleteCategory(int position){
        categories.remove(position);
    }

    private void fillCategories(){
        categories = new ArrayList<>();
        categories.add(new CategoryNote("Default 1", R.color.red));
        categories.add(new CategoryNote("Default 2", R.color.orange));
        categories.add(new CategoryNote("Default 3", R.color.green));
        categories.add(new CategoryNote("Default 4", R.color.blue));
        //categories.add(new CategoryNote("Create a new category", R.color.auto_hint));
    }

    private void showMenu(){
        isMenuOpen = true;
        changeVisibility();

        fabMenu.animate().rotation(45.0f);
        anim(fabAddNote, -120.0f, 1.0f, 1.0f);
        anim(textViewAddNote, -120.f, 1.0f, 1.0f);
        anim(fabCreateCategory, -240.0f, 1.0f, 1.0f);
        anim(textViewCreateCategory, -240.f, 1.0f, 1.0f);
        anim(fabRefreshView, -360.0f, 1.0f, 1.0f);
        anim(textViewRefreshView, -360.0f, 1.0f, 1.0f);
        anim(fabSaveData, -480.0f, 1.0f, 1.0f);
        anim(textViewSaveData, -480.0f, 1.0f, 1.0f);
    }


    private void closeMenu(){
        isMenuOpen = false;
        fabMenu.animate().rotation(0);
        anim(fabAddNote, 0, 0.5f, 0.5f);
        anim(fabAddNote, 0, 0.5f, 0.5f);
        anim(textViewAddNote, 0, 0.5f, 0.5f);
        anim(fabCreateCategory, 0, 0.5f, 0.5f);
        anim(textViewCreateCategory, 0, 0.5f, 0.5f);
        anim(fabRefreshView, 0, 0.5f, 0.5f);
        anim(textViewRefreshView, 0, 0.5f, 0.5f);
        anim(fabSaveData, 0, 0.5f, 0.5f);
        anim(textViewSaveData, 0, 0.5f, 0.5f);

        new CountDownTimer(300, 100){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                changeVisibility();
            }
        }.start();
    }

    private void changeVisibility(){
        if(isMenuOpen){
            fabAddNote.setVisibility(View.VISIBLE);
            textViewAddNote.setVisibility(View.VISIBLE);
            fabCreateCategory.setVisibility(View.VISIBLE);
            textViewCreateCategory.setVisibility(View.VISIBLE);
            fabRefreshView.setVisibility(View.VISIBLE);
            textViewRefreshView.setVisibility(View.VISIBLE);
            fabSaveData.setVisibility(View.VISIBLE);
            textViewSaveData.setVisibility(View.VISIBLE);
        }else{
            fabAddNote.setVisibility(View.INVISIBLE);
            textViewAddNote.setVisibility(View.INVISIBLE);
            fabCreateCategory.setVisibility(View.INVISIBLE);
            textViewCreateCategory.setVisibility(View.INVISIBLE);
            fabRefreshView.setVisibility(View.INVISIBLE);
            textViewRefreshView.setVisibility(View.INVISIBLE);
            fabSaveData.setVisibility(View.INVISIBLE);
            textViewSaveData.setVisibility(View.INVISIBLE);
        }
    }

    private void anim(View view, float dist, float scale, float alpha){
        view.animate().translationY(dist).scaleX(scale).scaleY(scale).alpha(alpha);
    }

}
