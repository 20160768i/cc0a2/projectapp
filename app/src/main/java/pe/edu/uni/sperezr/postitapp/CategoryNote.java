package pe.edu.uni.sperezr.postitapp;

import java.io.Serializable;
import java.util.ArrayList;

public class CategoryNote implements Serializable {
    private String title;
    private int color;
    private ArrayList<Note> notes;

    public CategoryNote(String title, int color) {
        this.title = title;
        this.color = color;
        notes = new ArrayList<>();
    }

    public CategoryNote(String title, int color, ArrayList<Note> notes) {
        this.title = title;
        this.color = color;
        this.notes = notes;
    }

    public ArrayList<String> arrayNotesTitles(){
        if(notes.isEmpty())
            return null;
        ArrayList<String> ret = new ArrayList<>();
        for (Note note:
             notes) {
            ret.add(note.getTitle());
        }
        return ret;
    }

    public boolean addNote(Note note){
        return notes.add(note);
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public ArrayList<Note> getNotes() {
        return notes;
    }

    public void setNotes(ArrayList<Note> notes) {
        this.notes = notes;
    }
}