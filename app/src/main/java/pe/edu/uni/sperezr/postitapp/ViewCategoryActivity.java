package pe.edu.uni.sperezr.postitapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.TextView;

public class ViewCategoryActivity extends AppCompatActivity {

    TextView textViewCategoryTitle;
    GridView gridView;
    int number;

    GridAdapterNotes gridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_category);

        textViewCategoryTitle = findViewById(R.id.text_view_category_title);
        gridView = findViewById(R.id.grid_view_notes);

        number = getIntent().getIntExtra("position_number",0);
        gridAdapter = new GridAdapterNotes(this, number);
        gridView.setAdapter(gridAdapter);

        textViewCategoryTitle.setText(MainActivity.categories.get(number).getTitle());

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(ViewCategoryActivity.this, ViewNoteActivity.class);
            intent.putExtra("number_to_note", number);
            intent.putExtra("position_to_note", position);
            startActivity(intent);
        });

        gridView.setOnItemLongClickListener((parent, view, position, id) -> {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle(R.string.dialog_title)
                    .setMessage(R.string.dialog_text)
                    .setNegativeButton(R.string.no, (dialog, which) -> dialog.cancel())
                    .setPositiveButton(R.string.yes, (dialog, which) -> {
                        deleteNote(position);
                        gridAdapter.notifyDataSetChanged();
                    })
                    .show();
            alertDialog.create();
            return true;
        });
    }
    private void deleteNote(int position){
        MainActivity.categories.get(number).getNotes().remove(position);
    }
}