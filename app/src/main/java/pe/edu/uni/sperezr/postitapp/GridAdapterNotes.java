package pe.edu.uni.sperezr.postitapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

public class GridAdapterNotes extends BaseAdapter {

    Context context;
    int position;

    public GridAdapterNotes(Context context, int position) {
        this.context = context;
        this.position = position;
    }

    @Override
    public int getCount() {
        return MainActivity.categories.get(position).getNotes().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_category_layout, parent, false);
        ImageView imageViewCircle = view.findViewById(R.id.image_view_note_color);
        TextView textViewTitle = view.findViewById(R.id.text_view_note_content);
        TextView textViewContent = view.findViewById(R.id.text_view_note_title);

        int color = MainActivity.categories.get(this.position).getColor();
        if(color < 0) //Color as aRBG
            imageViewCircle.setImageTintList(ColorStateList.valueOf(color));
        else //Color as Resource
            imageViewCircle.setImageTintList(ColorStateList.valueOf(ContextCompat.getColor(context, color)));
        textViewTitle.setText(MainActivity.categories.get(this.position).getNotes().get(position).getContent());
        textViewContent.setText(MainActivity.categories.get(this.position).getNotes().get(position).getTitle());
        return view;
    }
}
