package pe.edu.uni.sperezr.postitapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

public class CreateCategoryActivity extends AppCompatActivity {

    EditText editTextCategoryName;
    ImageButton imageButtonRed, imageButtonOrange, imageButtonGreen, imageButtonBlue, imageButtonCustom;
    ImageButton[] imageButtons;
    Button buttonCreateCategory;

    int colorChosen = 0;
    int customColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_category);

        editTextCategoryName = findViewById(R.id.edit_text_category_name);
        imageButtonRed = findViewById(R.id.image_button_red);
        imageButtonOrange = findViewById(R.id.image_button_orange);
        imageButtonGreen = findViewById(R.id.image_button_green);
        imageButtonBlue = findViewById(R.id.image_button_blue);
        imageButtonCustom = findViewById(R.id.image_button_custom);
        buttonCreateCategory = findViewById(R.id.button_create_category);

        initArray();
        setListeners();
        imageButtons[colorChosen].setBackgroundTintList(ColorStateList.valueOf(
                ContextCompat.getColor(CreateCategoryActivity.this, R.color.auto_hint)));

        imageButtonCustom.setOnClickListener(v -> ColorPickerDialogBuilder
                .with(v.getContext())
                .setTitle("Choose color")
                .initialColor(R.color.green)
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener(selectedColor -> {})
                .setPositiveButton(R.string.toast_msg_apply, (d, lastSelectedColor, allColors) -> {
                    customColor = lastSelectedColor;
                    if(colorChosen != 4) {
                        imageButtons[colorChosen].setBackgroundTintList(ColorStateList.valueOf(
                                ContextCompat.getColor(CreateCategoryActivity.this, R.color.transparent)));
                        colorChosen = 4;
                    }
                    imageButtonCustom.setBackgroundTintList(ColorStateList.valueOf(customColor));
                }).setNegativeButton(R.string.toast_msg_cancel, (dialog, which) -> {})
                .build()
                .show());

        buttonCreateCategory.setOnClickListener(v -> {
            String sName = editTextCategoryName.getText().toString();
            if(sName.isEmpty()) {
                Toast.makeText(getApplicationContext(), R.string.toast_msg_note_title_is_empty, Toast.LENGTH_LONG).show();
                return;
            }

            if(MainActivity.categories.add(new CategoryNote(sName, getColor()))){
                Toast.makeText(getApplicationContext(), R.string.toast_msg_category_created, Toast.LENGTH_SHORT).show();
                CreateNoteActivity.updateAdapter();
                finish();
            }else{
                Toast.makeText(getApplicationContext(), R.string.toast_msg_category_cannot_created, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initArray(){
        imageButtons = new ImageButton[4];
        imageButtons[0] = imageButtonRed;
        imageButtons[1] = imageButtonOrange;
        imageButtons[2] = imageButtonGreen;
        imageButtons[3] = imageButtonBlue;
    }

    private void setListeners(){
        for (int i = 0; i < imageButtons.length; i++) {
            int finalI = i;
            imageButtons[i].setOnClickListener(v -> {
                if(finalI != colorChosen){
                    if(colorChosen != 4) {
                        imageButtons[colorChosen].setBackgroundTintList(ColorStateList.valueOf(
                                ContextCompat.getColor(CreateCategoryActivity.this, R.color.transparent)));
                    }
                    imageButtons[finalI].setBackgroundTintList(ColorStateList.valueOf(
                            ContextCompat.getColor(CreateCategoryActivity.this, R.color.auto_hint)));
                    colorChosen = finalI;
                }
            });
        }
    }
    private int getColor(){
        switch(colorChosen){
            case 0: return R.color.red;
            case 1: return R.color.orange;
            case 2: return R.color.green;
            case 3: return R.color.blue;
            case 4: return customColor;
            default: return 0;
        }
    }
}
